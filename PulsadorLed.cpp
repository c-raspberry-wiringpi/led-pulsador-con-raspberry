#include <iostream>		
#include <wiringPi.h>


using namespace std;		

int main()
{
	wiringPiSetup(); // Se inicializa la libreria WiringPi
	pinMode(29, OUTPUT); //Led 29 como salida = Led
	pinMode(28, INPUT); // Led 28 como entrada = pulsador
	
	while(28) //Mientras pin 28 = Pulsador
	
	if(digitalRead(28) == 1) // Como se uso resistencia de Pull-Up este estado sera 1
	{                        // Cuando se presiona el pulsador cambia de estado a 0 
		digitalWrite(29, 1); // Si es 1 = Led Encendido
	}
	else
	{
		digitalWrite(29, 0); // Si es 0 = Led apagado
	}
	return 0;
}

